FROM ubuntu
RUN apt-get update 
RUN apt-get install python python-pip -y
RUN pip install flask
COPY flask_web.py /opt/
CMD ["python", "/opt/flask_web.py"]
EXPOSE 8888
